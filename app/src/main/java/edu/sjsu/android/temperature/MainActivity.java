package edu.sjsu.android.temperature;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (EditText) findViewById(R.id.editText);
    }

    // this method is called at button click because we assigned the name to the "onClick property of the button
    public void onClick(View view){
        switch (view.getId()){
            case R.id.button2:
            RadioButton celsiusButton = (RadioButton) findViewById(R.id.radioButton3);
            RadioButton fahrenheitButton = (RadioButton) findViewById(R.id.radioButton4);
            if(text.getText().length() == 0){
                Toast.makeText(this, "Please enter a valid number",
                Toast.LENGTH_LONG).show();
                return;
            }
            float inputValue = Float.parseFloat(text.getText().toString());
            if(celsiusButton.isChecked()){
                text.setText(String.valueOf(ConverterUtil.convertFahrenheitToCelsius(inputValue)));
                celsiusButton.setChecked(true);
            }else{
                text.setText(String.valueOf(ConverterUtil.convertCelsiusToFahrenheit(inputValue)));
                fahrenheitButton.setChecked(false);
                celsiusButton.setChecked(true);
            }
            break;
        }
    }

}
