package edu.sjsu.android.temperature;

public class ConverterUtil {
        // converts to celsius
        public static float convertFahrenheitToCelsius(float fahrenheit){
            return ((fahrenheit -32) * 5 / 9);
        }

        // converts to fahrenheit
        public static float convertCelsiusToFahrenheit(float celuius){
            return ((celuius * 9) / 5 ) + 32;
        }
}
